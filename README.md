## Chris's README

Hi, I'm Chris Moberly - Senior Manager for our [Security Operations](https://handbook.gitlab.com/handbook/security/security-operations/) group. I look after our offensive-security teams (Red Team and Threat Intelligence). I also help out a bit with SIRT and Trust & Safety.

I hope this page helps folks get to know me and what its like to work with me.

Please feel free to contribute to this page by opening a merge request.

## Related pages

Here are some of the tools I use to get my job done:

- [My 1:1 template](https://docs.google.com/document/d/1GhVowv-orotr0L6BtCosSIUwCDQxAW-t8y-ZOmhfx7s/edit?usp=sharing): I use this for 1:1s with my team members.
- [My personal issue board](https://gitlab.com/groups/gitlab-com/-/boards/9110142?assignee_username=cmoberly): I'm trying a new approach with this one, scoping to all issues assigned to me and using a sub-set of existing `workflow::` labels.

Here are some resources to learn more about my teams:

- [GitLab Handbook - Red Team](https://about.gitlab.com/handbook/engineering/security/threat-management/red-team/)
- [Red Team Public](https://gitlab.com/gitlab-com/gl-security/threatmanagement/redteam/redteam-public): Various resources we'd like to share with the community.
- [GitLab Handbook - Threat Intelligence](https://handbook.gitlab.com/handbook/security/security-operations/threat-intelligence/)
- [Threat Intelligence Public](https://gitlab.com/gitlab-com/gl-security/security-operations/threat-intelligence-public)

Here are some of the blogs I've published at GitLab:

- [Stealth operations: The evolution of GitLab's Red Team](https://about.gitlab.com/blog/2023/11/20/stealth-operations-the-evolution-of-gitlabs-red-team/)
- [How we run Red Team operations remotely](https://about.gitlab.com/blog/2022/05/11/how-we-run-red-team-operations-remotely/)
- [Post exploitation tactics in Google Cloud Platform](https://about.gitlab.com/blog/2020/02/12/plundering-gcp-escalating-privileges-in-google-cloud-platform/)
- [Why are developers so vulnerable to drive-by attacks?](https://about.gitlab.com/blog/2021/09/07/why-are-developers-vulnerable-to-driveby-attacks/)
- [Monitor your web attack surface with GitLab CI/CD and GitLab Pages](https://about.gitlab.com/blog/2023/01/11/monitor-web-attack-surface-with-gitlab/)
- [Use GitLab and MITRE ATT&CK Navigator to visualize adversary techniques](https://about.gitlab.com/blog/2023/08/09/gitlab-mitre-attack-navigator/)

## About me

Here are some things about me:

- I've zig-zagged around in my career a bit. From making coffee to providing tech support at that same coffee company, to systems administration, to professional services, to technical presales, to designing and delivering training programs, to managing technical alliances between startups and big tech companies, to pentesting, to red-teaming, and now to security management.
- I love every single Matrix movie. Yep, all of them.
- My first thought when starting this README was _"this would make great [OSINT](https://en.wikipedia.org/wiki/Open-source_intelligence) for someone looking to phish me."_ If that is you, well - congrats on your dedication. :)

## Communicating with me

- I would really appreciate any feedback that you think might help me do my job better. I won't be offended. Seriously, I mean it. Drop me a message any time.
- I would absolutely love to help anyone who has questions about security in general, making career transitions into the industry, or anything else you might want to ask. Please feel free to reach out about anything, I would be thrilled to hear from you.
- I can be fairly straight-faced when listening and processing information. It doesn't mean I'm upset or disagree, just that I'm trying carefully to understand and think through the ideas.
- Often, in written communication, I catch myself being a bit dry and factual. This is usually because I am aiming for clarity and accuracy. If I forget an emoji, just assume it's a positive one. :)
- I'm a hacker at heart. Often, my first instinct when hearing a new idea is to imagine ways it could go wrong, or how it could be bypassed or abused. This type of adversarial thinking can be a strength when focusing on systems, but can be easy to misinterpret as disagreement in some conversations. Feel free to mention this if you catch me dwelling on risks - let's work together to making things go right.
- I'm in Australia, and sometimes it's tough to find overlap for synchronous chats with team members in other regions. I can be flexible! If you can't find a spot in my calendar, ping me on Slack and we can work something out.

## Frequently-Asked Questions

### *How can I transition into security, or into offensive-security from a related field?*

This is something I did myself, and later in my career than many others! I was asked this same question in an interview years ago, you can [watch it here](https://www.youtube.com/watch?v=k7OsV_4LeJY). Here are some tips distilled down to bullets:

* Create a public identity for yourself that showcases your interests and experience. This will automatically set you apart from the vast majority of others who are competing for the same opportunities. Your identity can consist of things like:
  * A personal blog where you write about things you are learning, things you have built, things you have hacked, etc. Don't overthink this! It's ok to share things that are very basic and intro-level.
  * A public code repository with things like security tooling, scripts to automate attacks, exploits you've written or modified, etc. Again - don't overthink it! You can start small and iterate over time. Having something is better than having nothing.
  * Public vulnerability disclosures. These are golden. My best disclosures came from focusing on software that I use personally and could download and test locally. Small open-source projects are a good place to start, as you may find a project that not many other researchers are looking at. If you are doing bug bounties, it would be good to work on programs that allow public disclosure. Whatever you do, be kind to developers and maintainers, and keep in mind how your approach to disclosure may be viewed by potential future employers.
* Learn a lot about something that interests you. This may sound obvious, but you will do much better in an interview if you are passionate and knowledgeable about something specific. Pick a single application, operation system, vulnerability class, programming language, etc. Dive deep and learn everything you can. Exploit some bugs as you go. Write about it and share it somewhere public. When you get the inevitable *"tell me about a time you xxx..."* question in an interview, talk their ear off about it.
* Be proactive in your search for opportunities - do not wait for something to come to you. When searching for my first pentesting role, most of the conversations I had with future employers came from me reaching out to someone directly and not from applying to a job posting.
* Use your existing experience to set you apart. People who have built, managed, or protected systems have a unique insight that will help them when attacking those same systems. Maybe you have some other skills that give you a unique edge. Think about this and be prepared to explain why your non-security-experience is valuable in whatever role you are after.
* **"Be Visibile"**. This is the most common piece of advice I receive myself. Make sure people know who you are and are aware of the interesting things you are working on. This is especially relevant when trying to make a transition inside your organization. If no one knows who you are, they will never present you with opportunities. Here are some ideas to make that happen:
  * Participate in asynchronous communications relevant to the groups you are interested in. At GitLab, that would mean joining security Slack channels, commenting in issues, contributing in MRs, etc.
  * Participate in cross-function groups to solve business challenges.
  * Be the "go-to" person for your own specialty when it comes to security-related concerns.
  * Join your local security meetups, chat groups, and online forums. Meet people and talk to them. Let them know what you are looking to do and ask them for advice.

### *How can I get started with hacking?*

This is such a broad question, it's hard to answer without knowing a little bit more about the person asking it.

I learn how to build and manage something before I try breaking it. For example, when I began my research into Google Cloud post-exploitation techniques, I first completed a series of official trainings from Google designed for GCP administrators. I find it incredibly important to understand how a system works and why it works that way before I try to abuse it.

Without knowing how a system works, you'll struggle to find meaningful ways to exploit and abuse it.

Unless you already have a background in software development or systems administration, I highly recommend learning some basics first. Install Linux on an old laptop and learn how to navigate on the command line. Write some small scripts in Python, build a mobile app, experiment with a game engine like Unity or Unreal, etc. Do something that will put you in a position to troubleshoot, which is the best way to learn.

While you are learning these basics, you might find a certain area that interests you. Learn as much as you can about that area, and then start approaching it from an adversarial perspective. First build, then break.

With that out of the way, here are some good resources for hands-on hacking:

Web apps:
- [PentesterLab](https://pentesterlab.com/) (online training, free + paid)
- [Web Security Academy](https://portswigger.net/web-security) (online training, free)

Infrastructure:
- [The Hacker Playbook](https://www.goodreads.com/book/show/40028366-the-hacker-playbook-3) (book)
- [TryHackMe](https://tryhackme.com/) (online training)
- [HackTheBox](https://www.hackthebox.com/) (online training)
- [How Linux Works](https://www.goodreads.com/book/show/514432.How_Linux_Works) (book - not hacking but stellar resource)

Red Teaming:
- [MITRE ATT&CK: Getting Started](https://attack.mitre.org/resources/getting-started/) (blogs, papers)
- [Red Team Development & Operations](https://redteam.guide/) (book)

### *Do you have any tips for new team members at GitLab?*

* Anything that I spend time on, I try to make sure it is logged in an issue or merge request. The benefit of this is that I can go to [my activity feed](https://gitlab.com/users/cmoberly/activity) and get a nice overview of what I've been up to. This is really helpful when providing weekly updates, having 1:1s, doing self-evaluations, etc.
* Make handbook contributions. This is a great way to be visibile across the organization. If you have a question that can't be answered by the handbook, someone else will probably benefit from it being recorded as well.
* Treat all written communication at GitLab like it is public to the world or will eventually be made public. Think about how people could interpret or react to your statements, especially those with less context than you.
* Handbook MRs will automatically generate a review app. This gives you a great way to view your contribution as it will look in production. There is no need to build the handbook locally on your machine - combining the WebIDE with review apps gives you the complete experience, all in your browser.
